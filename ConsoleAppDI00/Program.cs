﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleAppDI00
{
    class Program
    {
        static void Main(string[] args)
        {
            Ilogger logger;
            //Read the type of logger from config file.
            string loggerType = "text";

            switch (loggerType)
            {
                case "database":
                    logger = new DatabaseLogger();
                    break;
                
                default:
                    logger = new TextLogger();
                    break;
            }

            LogMamager logMamager = new LogMamager(logger);
            try
            {
                throw new DivideByZeroException();
            }
            catch (Exception e)
            {
                logger.log(e.Message);
                Console.ReadLine();
            }
        }
    }

    interface Ilogger
    {
        void log(string message);
    }

    class LogMamager
    {
        private Ilogger _logger;
        public LogMamager(Ilogger logger)
        {
            _logger = logger;
        }

        public void Log(string message)
        {
            _logger.log(message);
        }
    }
    class TextLogger:Ilogger
    {
        public void log(string message)
        {
            Console.WriteLine("Log to a text file: " + message);
        }
    }

    class DatabaseLogger:Ilogger
    {
        public void log(string message)
        {
            Console.WriteLine("Log to a database: " + message);
        }
    }
}
